int M1d1 = 23, M1d2 = 25, M1pwm = 4; //M1 is the first motor, d1,d2 are the direction pins(2,6 respectively), and pwm is for the pwm pin(4)
int M2d1 = 27, M2d2 = 29, M2pwm = 5; //M2 is the second motor, d1,d2 are the direction pins(2,6 respectively), and pwm is for the pwm pin(4)
int M3d1 = 33, M3d2 = 31, M3pwm = 3; //M3 is the thirdmotor, d1,d2 are the direction pins(2,6 respectively), and pwm is for the pwm pin(4)
int M4d1 = 37, M4d2 = 35, M4pwm = 2; //M4 is the fourth motor, d1,d2 are the direction pins(2,6 respectively), and pwm is for the pwm pin(4)

#define LSA1_an A15
#define LSA2_an A14

double Setpoint, Input, Output;

double Kp=2, Ki=5, Kd=1;
PID myPID(&Input, &Output, &Setpoint, Kp, Ki, Kd, DIRECT);

void setup()
{
  //initialize the variables we're linked to
  Input = analogRead(LSA1_an);
  Setpoint = 100;

  //turn the PID on
  myPID.SetMode(AUTOMATIC);
}

void loop()
{
  Input = analogRead(PIN_INPUT);
  myPID.Compute();
  analogWrite(PIN_OUTPUT, Output);
}
