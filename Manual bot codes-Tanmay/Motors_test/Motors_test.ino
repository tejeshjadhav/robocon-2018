int M1d1 = 25, M1d2 = 23
, M1pwm = 4; //M1 is the first motor, d1,d2 are the direction pins(2,6 respectively), and pwm is for the pwm pin(4)
int M2d1 = 27, M2d2 = 29, M2pwm = 5; //M2 is the second motor, d1,d2 are the direction pins(2,6 respectively), and pwm is for the pwm pin(4)
int M3d1 = 31, M3d2 = 33, M3pwm = 3; //M3 is the thirdmotor, d1,d2 are the direction pins(2,6 respectively), and pwm is for the pwm pin(4)
int M4d1 = 37, M4d2 = 35, M4pwm = 2; //M4 is the fourth motor, d1,d2 are the direction pins(2,6 respectively), and pwm is for the pwm pin(4)
float M1p, M2p, M3p, M4p;

void setup() {
  // put your setup code here, to run once:
pinMode(M1d1, OUTPUT);
  pinMode(M1d2, OUTPUT);
  pinMode(M1pwm, OUTPUT);
  pinMode(M2d1, OUTPUT);
  pinMode(M2d2, OUTPUT);
  pinMode(M2pwm, OUTPUT);
  pinMode(M3d1, OUTPUT);
  pinMode(M3d2, OUTPUT);
  pinMode(M3pwm, OUTPUT);
  pinMode(M4d1, OUTPUT);
  pinMode(M4d2, OUTPUT);
  pinMode(M4pwm, OUTPUT);

  digitalWrite(M1d1, LOW);
  digitalWrite(M1d2, LOW);
  analogWrite(M1pwm, LOW);
  digitalWrite(M2d1, LOW);
  digitalWrite(M2d2, LOW);
  analogWrite(M2pwm, LOW);
  digitalWrite(M3d1, LOW);
  digitalWrite(M3d2, LOW);
  analogWrite(M3pwm, LOW);
  digitalWrite(M4d1, LOW);
  digitalWrite(M4d2, LOW);
  analogWrite(M4pwm, LOW);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(M1d1,HIGH);
  digitalWrite(M1d2,LOW);
  analogWrite(M1pwm,100);
  delay(3000);
  
    digitalWrite(M2d1,HIGH);
  digitalWrite(M2d2,LOW);
  analogWrite(M2pwm,100);
    delay(3000);
    
    digitalWrite(M3d1,HIGH);
  digitalWrite(M3d2,LOW);
  analogWrite(M3pwm,100);
    delay(3000);
    
    digitalWrite(M4d1,HIGH);
  digitalWrite(M4d2,LOW);
  analogWrite(M4pwm,100);
    delay(3000);
}
