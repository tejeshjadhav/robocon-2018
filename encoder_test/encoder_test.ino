#define Kp 50
#define MaxSpeed 255
#define offsetSpeed 10

int M1d1 = 25, M1d2 = 23, M1pwm = 9; //M1 is the first motor, d1,d2 are the direction pins(23,25 respectively), and pwm is for the pwm pin(40)
int M2d1 = 27, M2d2 = 29, M2pwm = 8; //M2 is the second motor, d1,d2 are the direction pins(29,27 respectively), and pwm is for the pwm pin(38)
int M3d1 = 31, M3d2 = 33, M3pwm = 7; //M3 is the thirdmotor, d1,d2 are the direction pins(31,33 respectively), an 09 o00the fourth motor, d1,d2 are the direction pins(37,35 respectively), and pwm is for the pwm pin(42)
float M1p, M2p, M3p, M4p;
#define encoderp1 21
#define encoderp2 20//v
#define encoderp3 19
#define encoderp4 18

int MaxSetSpeed = 60;

int countM1=0,countM2=0,countM3=0,countM4=0;
int sensorValF = 0;
int lastErrorF = 0;
int PwmToMotorsF = 0;
int LeftMotorSpeedF = 0, prevLeftMotorSpeedF = 0;
int RightMotorSpeedF = 0, prevRightMotorSpeedF = 0;

int sensorValB = 0;
int lastErrorB = 0;
int PwmToMotorsB = 0;
int LeftMotorSpeedB = 0, prevLeftMotorSpeedB = 0;
int RightMotorSpeedB = 0, prevRightMotorSpeedB = 0;

void setup()
{
 Serial.begin(9600);
attachInterrupt(digitalPinToInterrupt(encoderp1),M1interrupt,RISING);
attachInterrupt(digitalPinToInterrupt(encoderp2),M2interrupt,RISING);
attachInterrupt(digitalPinToInterrupt(encoderp3),M3interrupt,RISING);
attachInterrupt(digitalPinToInterrupt(encoderp4),M4interrupt,RISING);
//pinMode(trigpin, OUTPUT);
//  pinMode(echo, INPUT);
  pinMode(M1d1, OUTPUT);
  pinMode(M1d2, OUTPUT);
  pinMode(M1pwm, OUTPUT);
  pinMode(M2d1, OUTPUT);
  pinMode(M2d2, OUTPUT);
  pinMode(M2pwm, OUTPUT);
  pinMode(M3d1, OUTPUT);
  pinMode(M3d2, OUTPUT);
  pinMode(M3pwm, OUTPUT);
  pinMode(M4d1, OUTPUT);
  pinMode(M4d2, OUTPUT);
  pinMode(M4pwm, OUTPUT);

  digitalWrite(M1d1, LOW);
  digitalWrite(M1d2, LOW);
  analogWrite(M1pwm, LOW);
  digitalWrite(M2d1, LOW);
  digitalWrite(M2d2, LOW);
  analogWrite(M2pwm, LOW);
  digitalWrite(M3d1, LOW);
  digitalWrite(M3d2, LOW);
  analogWrite(M3pwm, LOW);
  digitalWrite(M4d1, LOW);
  digitalWrite(M4d2, LOW);
  analogWrite(M4pwm, LOW);
}

void _move(int leftPwmF, int rightPwmF,int leftPwmB,int rightPwmB)
{ 
  digitalWrite(M1d1, HIGH);
  digitalWrite(M1d2, LOW);
  analogWrite(M1pwm, leftPwmF);
  //analogWrite(M1pwm, 50);
  
 digitalWrite(M2d1, HIGH);
  digitalWrite(M2d2, LOW);
  analogWrite(M2pwm, rightPwmF);
  //analogWrite(M2pwm, 50);

  digitalWrite(M3d1, HIGH);
  digitalWrite(M3d2, LOW);
  analogWrite(M3pwm, rightPwmB);
 //analogWrite(M3pwm, 50);

  digitalWrite(M4d1, HIGH);
  digitalWrite(M4d2, LOW);
 analogWrite(M4pwm, leftPwmB);
 //analogWrite(M4pwm, 50);
 
}
void forward()
{
  
  int kp=5;
  int ref;
  static int lmbo=0,rmfo=0,rmbo=0,lmfo=0;
  ref=countM3;
  /*if(countM2<countM1)
  {
    rmfo+=3;.
  }
  else rmfo-=3;
   if(countM3<countM1)
  {
    rmbo+=3;
  }
  else rmbo-=3;
   if(countM4<countM1)
  {
    lmbo+=3;
  }
  else lmbo-=3;*/
  rmfo=(ref-countM2)*kp;
  //rmbo=(countM1-countM3)*kp;
  lmbo=(ref-countM4)*kp;
  lmfo=(ref-countM1)*kp;
  RightMotorSpeedF=MaxSetSpeed+rmfo;
  LeftMotorSpeedB=MaxSetSpeed+lmbo;
  RightMotorSpeedB=MaxSetSpeed+rmbo;
  LeftMotorSpeedF=MaxSetSpeed+lmfo;
 
      if (LeftMotorSpeedF > MaxSpeed) LeftMotorSpeedF = MaxSpeed;
      if (RightMotorSpeedF > MaxSpeed) RightMotorSpeedF = MaxSpeed;
      if (LeftMotorSpeedF < 0)LeftMotorSpeedF = 0;
      if (RightMotorSpeedF < 0)RightMotorSpeedF = 0;
      
      if (LeftMotorSpeedB > MaxSpeed) LeftMotorSpeedB = MaxSpeed;
      if (RightMotorSpeedB > MaxSpeed) RightMotorSpeedB = MaxSpeed;
      if (LeftMotorSpeedB < 0)LeftMotorSpeedB = 0;
      if (RightMotorSpeedB < 0)RightMotorSpeedB = 0;
   _move(LeftMotorSpeedF, RightMotorSpeedF,LeftMotorSpeedB,RightMotorSpeedB);
  
}


void loop()
{
  //_move(0,0,0,0);
  forward();
  Serial.print(countM1);
  Serial.print("  ");
  Serial.print(countM2);
  Serial.print("  ");
  Serial.print(countM3);
  Serial.print("  ");
  Serial.print(countM4);
  Serial.println("  ");
  /* if(countM1>=23)
  {
    detachInterrupt(encoderp1);
  digitalWrite(M1d1, HIGH);
  digitalWrite(M1d2, HIGH);
  analogWrite(M1pwm,HIGH);
  delay(20000);
  }*/

// digitalWrite(M3d1,HIGH);
  //digitalWrite(M3d2,LOW);
  //analogWrite(M3pwm,100);
}

void M1interrupt()
{ 
  countM1++;
}
void M2interrupt()
{
  countM2++;
}
void M3interrupt()
{
  countM3++;
}
void M4interrupt()
{
  countM4++;
}

