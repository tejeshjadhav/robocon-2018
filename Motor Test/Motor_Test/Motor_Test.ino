int M1d1 = 23, M1d2 = 25, M1pwm = 8; //M1 is the first motor, d1,d2 are the direction pins(2,6 respectively), and pwm is for the pwm pin(4)
int M2d1 = 27, M2d2 = 29, M2pwm = 9; //M2 is the second motor, d1,d2 are the direction pins(2,6 respectively), and pwm is for the pwm pin(4)
int M3d1 = 31, M3d2 = 33, M3pwm = 7; //M3 is the thirdmotor, d1,d2 are the direction pins(2,6 respectively), and pwm is for the pwm pin(4)
int M4d1 = 37, M4d2 = 35, M4pwm = 6; //M4 is the fourth motor, d1,d2 are the direction pins(2,6 respectively), and pwm is for the pwm pin(4)



void setup() {

  
  pinMode(M1d1, OUTPUT);
  pinMode(M1d2, OUTPUT);
  pinMode(M1pwm, OUTPUT);
  pinMode(M2d1, OUTPUT);
  pinMode(M2d2, OUTPUT);
  pinMode(M2pwm, OUTPUT);
  pinMode(M3d1, OUTPUT);
  pinMode(M3d2, OUTPUT);
  pinMode(M3pwm, OUTPUT);
  pinMode(M4d1, OUTPUT);
  pinMode(M4d2, OUTPUT);
  pinMode(M4pwm, OUTPUT);

  digitalWrite(M1d1, LOW);
  digitalWrite(M1d2, LOW);
  analogWrite(M1pwm, 255);
  digitalWrite(M2d1, LOW);
  digitalWrite(M2d2, LOW);
  analogWrite(M2pwm, 255);
  digitalWrite(M3d1, LOW);
  digitalWrite(M3d2, LOW);
  analogWrite(M3pwm, 255);
  digitalWrite(M4d1, LOW);
  digitalWrite(M4d2, LOW);
  analogWrite(M4pwm, 255);
}

void forward(int pwm) {
  digitalWrite(M1d1, HIGH);
  digitalWrite(M1d2, LOW);
  analogWrite(M1pwm, pwm);
  digitalWrite(M2d1, LOW);
  digitalWrite(M2d2, HIGH);
  analogWrite(M2pwm, pwm);
  digitalWrite(M3d1, LOW);
  digitalWrite(M3d2, HIGH);
  analogWrite(M3pwm, pwm);
  digitalWrite(M4d1, HIGH);
  digitalWrite(M4d2, LOW);
  analogWrite(M4pwm, pwm);
}

void loop ()
{
  forward(255);
}


