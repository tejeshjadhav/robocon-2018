/* Sweep
 by BARRAGAN <http://barraganstudio.com>
 This example code is in the public domain.

 modified 8 Nov 2013
 by Scott Fitzgerald
 http://www.arduino.cc/en/Tutorial/Sweep
*/

#include <Servo.h>

Servo myservo_claw;
Servo myservo_wrist;
Servo myservo_arm1;
Servo myservo_arm2;// create servo object to control a servo
                        // twelve servo objects can be created on most boards

int pos_claw = 0;
int pos_wrist = 0;
int pos_arm = 0;
// variable to store the servo position

void setup()
{
  myservo_claw.attach(6);
  myservo_wrist.attach(7);
  myservo_arm1.attach(9);
  myservo_arm2.attach(8);// attaches the servo on pin 9 to the servo object
}

void loop()
{
  for (pos_arm = 0; pos_arm <= 180; pos_arm += 1)
  { 
    // goes from 0 degrees to 180 degrees
    // in steps of 1 degree
    myservo_arm1.write(pos_arm);
    myservo_arm2.write(180-pos_arm);
    delay(10);
    pos_arm = pos_wrist;
    myservo_wrist.write(pos_wrist);                      // tell servo to go to position in variable 'pos'
    delay(1000);
    /*for(pos_claw = 0; pos_claw <= 90; pos_claw++)
    {
      myservo_claw.write(pos_claw);
    }
    delay(3000);
    for(pos_claw = 90; pos_claw >= 0; pos_claw--)
    {
      myservo_claw.write(pos_claw);
    }
    delay(3000);*/
  }                                                    // waits 15ms for the servo to reach the position
  
    for (pos_arm = 180; pos_arm >= 0; pos_arm -= 1) { // goes from 0 degrees to 180 degrees
    myservo_arm1.write(pos_arm);
    myservo_arm2.write(180-pos_arm);
    delay(10);
    pos_arm = pos_wrist;
    myservo_wrist.write(pos_wrist);                      // tell servo to go to position in variable 'pos'
    delay(1000);
    /*for(pos_claw = 0; pos_claw <= 90; pos_claw++)
    {
      myservo_claw.write(pos_claw);
    }
    delay(3000);
    for(pos_claw = 90; pos_claw >= 0; pos_claw--)
    {
      myservo_claw.wr ite(pos_claw);
    }
    delay(3000);*/
  }
}

