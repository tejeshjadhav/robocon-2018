#define Kp 50
#define MaxSpeed 255
#define offsetSpeed 10
//#define encoderp1
//#define encoderp2
//#define encoderp3
//#define encoderp4

int smoothedValF;
int smoothedValB;
float filterVal=0.55;
int sharpF = A3;
int sharpB = A5;
int setpoint = 5;
int errorF = 0;
int errorB = 0;
volatile unsigned int  countM1=0,countM2=0,countM3=0,countM4=0;

int sensorValF = 0;
int lastErrorF = 0;
int PwmToMotorsF = 0;
int LeftMotorSpeedF = 0, prevLeftMotorSpeedF = 0;
int RightMotorSpeedF = 0, prevRightMotorSpeedF = 0;

int sensorValB = 0;
int lastErrorB = 0;
int PwmToMotorsB = 0;
int LeftMotorSpeedB = 0, prevLeftMotorSpeedB = 0;
int RightMotorSpeedB = 0, prevRightMotorSpeedB = 0;

int MaxSetSpeed = 60;

int M1d1 = 25, M1d2 = 23, M1pwm = 4; //M1 is the first motor, d1,d2 are the direction pins(23,25 respectively), and pwm is for the pwm pin(40)
int M2d1 = 27, M2d2 = 29, M2pwm = 5; //M2 is the second motor, d1,d2 are the direction pins(29,27 respectively), and pwm is for the pwm pin(38)
int M3d1 = 31, M3d2 = 33, M3pwm = 3; //M3 is the thirdmotor, d1,d2 are the direction pins(31,33 respectively), and pwm is for the pwm pin(44)
int M4d1 = 37, M4d2 = 35, M4pwm = 2; //M4 is the fourth motor, d1,d2 are the direction pins(37,35 respectively), and pwm is for the pwm pin(42)
float M1p, M2p, M3p, M4p;


void setup()
{
  Serial.begin(9600);
 // attachInterrupt(digitalPinToInterrupt(encoderp1),M1interrupt,RISING);
  //attachInterrupt(digitalPinToInterrupt(encoderp2),M2interrupt,RISING);
  //attachInterrupt(digitalPinToInterrupt(encoderp3),M3interrupt,RISING);
  //attachInterrupt(digitalPinToInterrupt(encoderp4),M4interrupt,RISING);
  pinMode(M1d1, OUTPUT);
  pinMode(M1d2, OUTPUT);
  pinMode(M1pwm, OUTPUT);
  pinMode(M2d1, OUTPUT);
  pinMode(M2d2, OUTPUT);
  pinMode(M2pwm, OUTPUT);
  pinMode(M3d1, OUTPUT);
  pinMode(M3d2, OUTPUT);
  pinMode(M3pwm, OUTPUT);
  pinMode(M4d1, OUTPUT);
  pinMode(M4d2, OUTPUT);
  pinMode(M4pwm, OUTPUT);

  digitalWrite(M1d1, LOW);
  digitalWrite(M1d2, LOW);
  analogWrite(M1pwm, LOW);
  digitalWrite(M2d1, LOW);
  digitalWrite(M2d2, LOW);
  analogWrite(M2pwm, LOW);
  digitalWrite(M3d1, LOW);
  digitalWrite(M3d2, LOW);
  analogWrite(M3pwm, LOW);
  digitalWrite(M4d1, LOW);
  digitalWrite(M4d2, LOW);
  analogWrite(M4pwm, LOW);
  cli();
}
void _move(int leftPwmF, int rightPwmF,int leftPwmB,int rightPwmB)
{ 
  digitalWrite(M1d1, HIGH);
  digitalWrite(M1d2, LOW);
  analogWrite(M1pwm, leftPwmF);
  //analogWrite(M1pwm, 50);*/
  
  digitalWrite(M2d1, HIGH);
  digitalWrite(M2d2, LOW);
  analogWrite(M2pwm, rightPwmF);
  //analogWrite(M2pwm, 50);

  digitalWrite(M3d1, HIGH);
  digitalWrite(M3d2, LOW);
  analogWrite(M3pwm, rightPwmB);
 //analogWrite(M3pwm, 50);

  digitalWrite(M4d1, HIGH);
  digitalWrite(M4d2, LOW);
 analogWrite(M4pwm, leftPwmB);
 //analogWrite(M4pwm, 50);
 
}
void wallFollow()
{
  sensorValF=float(analogRead(sharpF)/50);
  sensorValB=float(analogRead(sharpB)/50);
   /* Serial.print(sensorValF);
    Serial.print("  ");
    Serial.print(sensorValB);
    Serial.print("  ");*/
  smoothedValF=smooth(sensorValF,filterVal,smoothedValF);
  smoothedValB=smooth(sensorValB,filterVal,smoothedValB);
    Serial.print(smoothedValF);
    Serial.print("  ");
    Serial.print(smoothedValB);
    Serial.print("  ");
   errorF = smoothedValF - setpoint;
    errorB = smoothedValB - setpoint;
    PwmToMotorsF = Kp * errorF*-1;
    PwmToMotorsB = Kp * errorB*-1;
      if (errorF<0 && errorF<=errorB)
      {
        LeftMotorSpeedF = MaxSetSpeed -PwmToMotorsF ;
        RightMotorSpeedF = MaxSetSpeed + PwmToMotorsF + offsetSpeed;
      
      if (LeftMotorSpeedF > MaxSpeed) LeftMotorSpeedF = MaxSpeed;
      if (RightMotorSpeedF > MaxSpeed) RightMotorSpeedF = MaxSpeed;
      if (LeftMotorSpeedF < 0)LeftMotorSpeedF = 0;
      if (RightMotorSpeedF < 0)RightMotorSpeedF = 0;
      }
    else
    {
      LeftMotorSpeedF = MaxSetSpeed;
      RightMotorSpeedF = MaxSetSpeed+offsetSpeed;
    }
    
     if (errorB<0 && errorB<=errorF)
      {
        LeftMotorSpeedB = MaxSetSpeed + PwmToMotorsB + offsetSpeed ;
        RightMotorSpeedB = MaxSetSpeed -PwmToMotorsB;
      
      if (LeftMotorSpeedB > MaxSpeed) LeftMotorSpeedB = MaxSpeed;
      if (RightMotorSpeedB > MaxSpeed) RightMotorSpeedB = MaxSpeed;
      if (LeftMotorSpeedB < 0)LeftMotorSpeedB = 0;
      if (RightMotorSpeedB < 0)RightMotorSpeedB = 0;
      }
    else
    {
      LeftMotorSpeedB = MaxSetSpeed+offsetSpeed;
      RightMotorSpeedB = MaxSetSpeed;
    }
     
}
void diagonal()
{
  sei();
  RightMotorSpeedF=MaxSetSpeed;
  LeftMotorSpeedB=MaxSetSpeed;
  if(countM1>countM4)
  {
    RightMotorSpeedF++;
    LeftMotorSpeedB--;
 
      if (LeftMotorSpeedF > MaxSpeed) LeftMotorSpeedF = MaxSpeed;
      if (RightMotorSpeedF > MaxSpeed) RightMotorSpeedF = MaxSpeed;
      if (LeftMotorSpeedF < 0)LeftMotorSpeedF = 0;
      if (RightMotorSpeedF < 0)RightMotorSpeedF = 0;
  }
  RightMotorSpeedB=0;
  LeftMotorSpeedF=0;
   _move(LeftMotorSpeedF, RightMotorSpeedF,LeftMotorSpeedB,RightMotorSpeedB);
  
}


void loop()
{
    
    wallFollow();
   
    Serial.print(LeftMotorSpeedF);
    Serial.print("  ");
    Serial.print(RightMotorSpeedF);
    Serial.print("  ");
    Serial.print(LeftMotorSpeedB);
    Serial.print("  ");
    Serial.print(RightMotorSpeedB);
    Serial.println("  ");
    _move(LeftMotorSpeedF, RightMotorSpeedF,LeftMotorSpeedB,RightMotorSpeedB);
}
/*void M1interrupt()
{
  countM1++;
}
void M2interrupt()
{
  countM2++;
}
void M3interrupt()
{
  countM3++;
}
void M4interrupt()
{
  countM4++;
}*/
int smooth(int data, float filterVal, float smoothedVal){


  if (filterVal > 1){      // check to make sure param's are within range
    filterVal = .99;
  }
  else if (filterVal <= 0){
    filterVal = 0;
  }

  smoothedVal = (data * (1 - filterVal)) + (smoothedVal  *  filterVal);

  return (int)smoothedVal;
}


